#!/bin/bash
opencv="opencv"
version="$(wget -q -O - http://sourceforge.net/projects/opencvlibrary/files/opencv-unix | egrep -m1 -o '\"[0-9](\.[0-9])+' | cut -c2-)"
ext=".zip"

echo "Installing OpenCV" $version

echo "Creating" $opencv "folder"
mkdir $opencv
cd $opencv

#echo "Removing any pre-installed ffmpeg and x264"
#sudo apt-get -qq remove ffmpeg x264 libx264-dev

echo "Installing Dependenices"
sudo apt-get install build-essential checkinstall cmake pkg-config yasm libtiff4-dev libjpeg-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libdc1394-22-dev libxine-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev python-dev python-numpy libtbb-dev libqt4-dev libgtk2.0-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils 

# install ffmpeg
sudo add-apt-repository ppa:mc3man/`lsb_release -sc`-media
sudo apt-get update
sudo apt-get install ffmpeg gstreamer0.10-ffmpeg

echo "Downloading OpenCV" $version
wget -N -O $opencv-$version$ext http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/$version/opencv-"$version$ext"/download

echo "Installing OpenCV" $version
#tar -xvf $opencv-$version.tar.gz
unzip $opencv-$version$ext
cd $opencv-$version

mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D BUILD_NEW_PYTHON_SUPPORT=ON -D WITH_V4L=ON -D INSTALL_C_EXAMPLES=ON -D INSTALL_PYTHON_EXAMPLES=ON -D BUILD_EXAMPLES=ON -D WITH_QT=ON -D WITH_OPENGL=ON -D WITH_GTK=ON ..
make -j2
sudo make install
sudo sh -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf'
sudo ldconfig
echo "OpenCV" $version "ready to be used"

