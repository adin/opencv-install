# About

An script to install OpenCV easily.

This script will download, configure dependencies, build, and install OpenCV.

To execute it, run from the folder you want OpenCV to be created, for example

```bash
mkdir ~/opencv
cd ~/opencv
~/path/to/opencv-install.sh
```

where `~/path/to/opencv-install.sh` is where you placed this script.
